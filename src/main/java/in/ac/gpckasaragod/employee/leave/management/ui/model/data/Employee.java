/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.employee.leave.management.ui.model.data;

/**
 *
 * @author student
 */
public class Employee {
    private Integer id;
    private String employeeName;
       private String employeeDepartment;
       private String employeeAddress; 
       private String employeeEmail;
       private String employeePhoneNo;

    public Employee(String employeeName, Integer employeeId, String employeeDepartment, String employeeAddress, String employeeEmail, String employeePhoneNo) {
        this.employeeName = employeeName;
        this.id = id;
        this.employeeDepartment = employeeDepartment;
        this.employeeAddress = employeeAddress;
        this.employeeEmail = employeeEmail;
        this.employeePhoneNo = employeePhoneNo;
    }

    public Employee(Integer Id, String employeeName, String employeeDepartment, String employeeAddress, String employeeEmail, String employeePhoneNo) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public Integer getId() {
        return id;
    }

    public String getEmployeeDepartment() {
        return employeeDepartment;
    }

    public String getEmployeeAddress() {
        return employeeAddress;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public String getEmployeePhoneNo() {
        return employeePhoneNo;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setEmployeeDepartment(String employeeDepartment) {
        this.employeeDepartment = employeeDepartment;
    }

    public void setEmployeeAddress(String employeeAddress) {
        this.employeeAddress = employeeAddress;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public void setEmployeePhoneNo(String employeePhoneNo) {
        this.employeePhoneNo = employeePhoneNo;
    }
}

   