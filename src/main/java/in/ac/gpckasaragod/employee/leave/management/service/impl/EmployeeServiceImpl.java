/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.employee.leave.management.service.impl;

import in.ac.gpckasaragod.employee.leave.management.service.EmployeeService;
import in.ac.gpckasaragod.employee.leave.management.ui.model.data.Employee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class EmployeeServiceImpl extends ConnectionServiceImpl implements EmployeeService{

   
@Override
public String saveEmployee(String employeeName,String department,String address,String email,String phoneNo){
       try {
           Connection connection = getConnection();
           Statement statement = connection.createStatement();
           String query = "INSERT INTO EMPINFO(EMPLOYEENAME,DEPARTMENT,ADDRESS,EMAIL,PHONENO) VALUES "+"('"+employeeName+"','"+department+"','"+address+"','"+email+"','"+phoneNo+"')";  
           
           System.err.println("Query:"+query);
           int status = statement.executeUpdate(query);
           if(status !=1){
               return "Save failed";
           }else{
               return"Saved succesfully";
               
           }
           
       } catch (SQLException ex){
           Logger.getLogger(EmployeeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
       }
       return "Save failed";
                   
       }
    
   
    @Override
       public Employee readEmployee(Integer Id){
           Employee employee = null;
          try{
              Connection connection = getConnection();
              Statement statement = connection.createStatement();
              String query="SELECT * FROM EMPINFO WHERE ID="+Id;
              ResultSet resultSet = statement.executeQuery(query);
              while(resultSet.next()){
                                  String employeeName = resultSet.getString("NAME");
                  String employeeDepartment = resultSet.getString("DEPARTMENT");
                  String employeeAddress = resultSet.getString("ADDRESS");
                  String employeeEmail = resultSet.getString("EMAIL");
                  String employeePhoneNo = resultSet.getString("PHONENO");
                  employee = new Employee(employeeName, Id, employeeDepartment, employeeAddress, employeeEmail, employeePhoneNo);
                  
                  
                  
              }
              
          } catch (SQLException ex){
              Logger.getLogger(EmployeeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
          }
        return employee;
       }

    @Override
    public List<Employee> getAllEmployees() {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
       List<Employee> employees = new ArrayList<>(); 
       try {
           Connection connection = getConnection();
           Statement statement = connection.createStatement();
           String query = "SELECT * FROM EMPINFO";
           ResultSet resultSet = statement.executeQuery(query);
           while(resultSet.next()){
           String employeeName = resultSet.getString("EMPLOYEENAME");
           int id = resultSet.getInt("ID");
           String employeeDepartment = resultSet.getString("DEPARTMENT");
           String employeeAddress = resultSet.getString("ADDRESS");
           String employeeEmail = resultSet.getString("EMAIL");
           String employeePhoneNo = resultSet.getString("PHONENO");
           Employee employee = new Employee(employeeName, id, employeeDepartment, employeeAddress, employeeEmail, employeePhoneNo);
           employees.add(employee);
       }
   
       
     } catch (SQLException ex){
              Logger.getLogger(EmployeeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
          }
        return employees;
       }

@Override
    public String updateEmployee(Integer Id, String txtName, String txtDepartment, String txtAddress, String txtEmail, String txtPhone) {
        try{
            Connection connection = getConnection();
             String query = "UPDATE EMPINFO SET EMPLOYEENAME=?,DEPARTMENT=?,ADDRESS=?,EMAIL=?,PHONENO=? WHERE ID=?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(6, Id);
            statement.setString(1, employeeName);
            statement.setString(2, eDepartment);
      
           
            
        } catch (SQLException ex) {
        Logger.getLogger(EmployeeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
    }

    @Override
    public String deleteEmployee(Integer selectId, String txtName, String txtDepartment, String txtAddress, String txtEmail, String txtPhone) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String deleteEmployee(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
   
}   
                                      
    
  









