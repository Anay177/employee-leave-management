/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.employee.leave.management.service;

import in.ac.gpckasaragod.employee.leave.management.ui.model.data.Employee;
import java.util.List;

/**
 *
 * @author student
 */
public interface EmployeeService {
    public String saveEmployee(String employeeName,String department,String Address,String Email,String phoneNo);
    public Employee readEmployee(Integer Id);
    public List<Employee> getAllEmployees();

    public String updateEmployee(Integer selectId, String txtName, String txtDepartment, String txtAddress, String txtEmail, String txtPhone);
    public String deleteEmployee(Integer selectId, String txtName, String txtDepartment, String txtAddress, String txtEmail, String txtPhone);
    public String deleteEmployee(Integer id);



   
    
    
}