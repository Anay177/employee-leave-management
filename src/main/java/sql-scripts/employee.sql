/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/SQLTemplate.sql to edit this template
 */
/**
 * Author:  student
 * Created: 08-Jun-2022
 */
CREATE DATABASE IF NOT EXISTS EMPLEAVE;
USE EMPLEAVE; 
CREATE TABLE IF NOT EXISTS EMPINFO(EMPLOYEENAME VARCHAR(20) NOT NULL,ID INT PRIMARY KEY AUTO_INCREMENT,DEPARTMENT VARCHAR(20) NOT NULL,ADDRESS VARCHAR(30) NOT NULL,EMAIL VARCHAR(20) NOT NULL,PHONENO VARCHAR(10) NOT NULL);

DESC EMPINFO;
SELECT * FROM EMPINFO;

